# templates webwml Catalan template.
# Copyright (C) 2002, 2003, 2004 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2005, 2010-2011, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2018-07-12 00:59+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Seguretat de Debian"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Avisos de seguretat de Debian"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "P"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"avisos de seguretat <a href=\\\"undated/\\\">sense data</a>, inclosos per a "
"la posteritat"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Diccionari CVE de Mitre"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Base de dades Bugtraq de Securityfocus"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "Avisos del CERT"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "Notes de les vulnerabilitats del US-CERT"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Font:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Component independent d'arquitectura:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"Les sumes de comprovació MD5 dels fitxers llistats estan disponibles a l'<a "
"href=\"<get-var url />\">avís original</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"Les sumes de comprovació MD5 dels fitxers llistats estan disponibles a l'<a "
"href=\"<get-var url />\">avís revisat</a>."

#: ../../english/template/debian/security.wml:42
msgid "Debian Security Advisory"
msgstr "Avís de seguretat de Debian"

#: ../../english/template/debian/security.wml:47
msgid "Date Reported"
msgstr "Data de l'informe"

#: ../../english/template/debian/security.wml:50
msgid "Affected Packages"
msgstr "Paquets afectats"

#: ../../english/template/debian/security.wml:72
msgid "Vulnerable"
msgstr "Vulnerable"

#: ../../english/template/debian/security.wml:75
msgid "Security database references"
msgstr "Referències de la base de dades de seguretat"

#: ../../english/template/debian/security.wml:78
msgid "More information"
msgstr "Més informació"

#: ../../english/template/debian/security.wml:84
msgid "Fixed in"
msgstr "Arreglat en"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "ID de BugTraq"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Error"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "En el sistema de seguiment d'errors de Debian:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "A la base de dades de Bugtraq (a SecurityFocus):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "Al diccionari CVE de Mitre:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "Vulnerabilitats, avisos i incidents del CERT:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
"Actualment no hi ha referències disponibles a altres bases de dades de "
"seguretat. "
