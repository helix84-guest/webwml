# German translation of the Debian webwml modules
# Dr. Tobias Quathamer <toddy@debian.org>, 2011, 2012, 2016, 2017, 2018.
# Holger Wansing <linux@wansing-online.de>, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2018-09-15 14:56+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Übersetzungsstatistik der Debian-Website"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Es sind %d Seiten zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Es sind %d Byte zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Es sind %d Zeichenketten zu übersetzen."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Falsche Version übersetzt"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Diese Übersetzung ist zu alt"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Das Original ist aktueller als diese Übersetzung"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Das Original existiert nicht mehr"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "Keine Zugriffszahlen"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "Zugriffe"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Klicken Sie, um diffstat-Daten zu erhalten"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Erstellt mit <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Zusammenfassung der Übersetzung für"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Veraltet"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Übersetzt"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Aktuell"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "Dateien"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "Byte"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Beachten Sie: Die Auflistung der Seiten ist nach der Häufigkeit der Abrufe "
"sortiert. Bewegen Sie den Mauszeiger auf den Seitennamen, um die Anzahl der "
"Zugriffe zu sehen."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Veraltete Übersetzungen"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Datei"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Unterschiede"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git-Befehlszeile"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Protokoll"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Übersetzung"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Betreuer"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Übersetzer"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Allgemeine Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Nicht übersetzte allgemeine Seiten"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nachrichten, die nicht übersetzt sind"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Nicht übersetzte Nachrichten"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Berater-/Benutzerseiten, die nicht übersetzt sind"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Nicht übersetzte Berater-/Benutzerseiten"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Internationale Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Nicht übersetzte internationale Seiten"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Übersetzte Seiten (aktuell)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Übersetzte Vorlagen (PO-Dateien)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Statistik der PO-Übersetzung"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Ungenau"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Insgesamt"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Insgesamt:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Übersetzte Webseiten"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Übersetzungsstatistik nach der Seitenanzahl"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Sprache"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Übersetzungen"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Übersetzte Webseiten (nach Größe)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Übersetzungsstatistik nach der Seitengröße"

#~ msgid "Created with"
#~ msgstr "Erzeugt mit"

#~ msgid "Origin"
#~ msgstr "Original"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Zugriffsdaten von %s, erfasst am %s."

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Farbige Unterschiede"

#~ msgid "Colored diff"
#~ msgstr "Farbige Unterschiede"

#~ msgid "Unified diff"
#~ msgstr "Einheitliche Unterschiede"
