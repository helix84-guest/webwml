<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple CSRF and XSS issues allow remote attackers to hijack the
authentication and execute roundcube operations without the consent of the
user. In some cases, this could result in data loss or data theft.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9587">CVE-2014-9587</a>

    <p>Multiple cross-site request forgery (CSRF) vulnerabilities in
    allow remote attackers to hijack the authentication of unspecified
    victims via unknown vectors, related to (1) address book operations or
    the (2) ACL or (3) Managesieve plugins.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1433">CVE-2015-1433</a>

    <p>Incorrect quotation logic during sanitization of style HTML
    attribute allows remote attackers to execute arbitrary
    javascript code on the user's browser.</p></li>

<a href="https://security-tracker.debian.org/tracker/CVE-2016-4069">CVE-2016-4069</a></p>

    <p>Cross-site request forgery (CSRF) vulnerability allows remote
    attackers to hijack the authentication of users for requests that
    download attachments and cause a denial of service (disk consumption)
    via unspecified vectors.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.7.2-9+deb7u4.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-613.data"
# $Id: $
