<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Berkeley Roshan Churchill reported a regression for the recent
security update for ghostscript, announced as DLA-1527-1, caused by an
incomplete
fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-16543">CVE-2018-16543</a>. 
The pdf2ps tool failed to produce any output and
aborted with /rangecheck in .installpagedevice error.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.06~dfsg-2+deb8u10.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1527-2.data"
# $Id: $
