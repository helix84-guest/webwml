<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Thunderbird mail
client: Multiple memory safety errors, use after free, integer overflows
and other implementation errors may lead to crashes or the execution of
arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:52.6.0-1~deb7u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1262.data"
# $Id: $
