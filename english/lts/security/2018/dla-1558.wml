<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16395">CVE-2018-16395</a>

      Fix for OpenSSL::X509::Name equality check.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16396">CVE-2018-16396</a>

      <p>Tainted flags are not propagated in Array#pack and String#unpack
      with some directives.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.5-2+deb8u6.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1558.data"
# $Id: $
