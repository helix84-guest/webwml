<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Ming:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11732">CVE-2017-11732</a>

    <p>heap-based buffer overflow vulnerability in the function dcputs
    (util/decompile.c) in Ming <= 0.4.8, which allows attackers to
    cause a denial of service via a crafted SWF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16883">CVE-2017-16883</a>

    <p>NULL pointer dereference vulnerability in the function outputSWF_TEXT_RECORD
    (util/outputscript.c) in Ming <= 0.4.8, which allows attackers
    to cause a denial of service via a crafted SWF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16898">CVE-2017-16898</a>

    <p>global buffer overflow vulnerability in the function printMP3Headers
    (util/listmp3.c) in Ming <= 0.4.8, which allows attackers to cause a
    denial of service via a crafted SWF file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.4.4-1.1+deb7u6.</p>

<p>We recommend that you upgrade your ming packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1240.data"
# $Id: $
