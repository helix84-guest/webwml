<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in qemu-kvm, a full
virtualization solution for Linux hosts on x86 hardware with x86 guests
based on the Quick Emulator(Qemu).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6505">CVE-2017-6505</a>

    <p>Denial of service via infinite loop in the USB OHCI emulation</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8309">CVE-2017-8309</a>

    <p>Denial of service via VNC audio capture</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10664">CVE-2017-10664</a>

    <p>Denial of service in qemu-nbd server, qemu-io and qemu-img.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11434">CVE-2017-11434</a>

    <p>Denial of service via a crafted DHCP options string</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u23.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1071.data"
# $Id: $
