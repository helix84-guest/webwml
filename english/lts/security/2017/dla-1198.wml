<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Leon Zhao discovered several security vulnerabilities in libextractor,
a universal library and command-line tool to obtain meta-data about
files. NULL Pointer Dereferences, heap-based buffer overflows, integer
signedness errors and out-of-bounds read may lead to a denial-of-service
(application crash) or have other unspecified impact.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.6.3-5+deb7u1.</p>

<p>We recommend that you upgrade your libextractor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1198.data"
# $Id: $
