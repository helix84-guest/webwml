<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in qemu-kvm, a full
virtualization solution for Linux hosts on x86 hardware with x86 guests
based on the Quick Emulator(Qemu).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9602">CVE-2016-9602</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper link
  following issue.  It could occur while accessing symbolic link files
  on a shared host directory.</p>

  <p>A privileged user inside guest could use this flaw to access host file
  system beyond the shared folder and potentially escalating their
  privileges on a host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7377">CVE-2017-7377</a>

  <p>Quick Emulator(Qemu) built with the virtio-9p back-end support is
  vulnerable to a memory leakage issue. It could occur while doing a I/O
  operation via v9fs_create/v9fs_lcreate routine.</p>

  <p>A privileged user/process inside guest could use this flaw to leak
  host memory resulting in Dos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7471">CVE-2017-7471</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper access
  control issue.  It could occur while accessing files on a shared host
  directory.</p>

  <p>A privileged user inside guest could use this flaw to access host file
  system beyond the shared folder and potentially escalating their
  privileges on a host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7493">CVE-2017-7493</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper access
  control issue.  It could occur while accessing virtfs metadata files
  in mapped-file security mode.</p>

  <p>A guest user could use this flaw to escalate their privileges inside
  guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8086">CVE-2017-8086</a>

  <p>Quick Emulator(Qemu) built with the virtio-9p back-end support is
  vulnerable to a memory leakage issue. It could occur while querying
  file system extended attributes via 9pfs_list_xattr() routine.</p>

  <p>A privileged user/process inside guest could use this flaw to leak
  host memory resulting in Dos.</p></li>

</ul>

For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u22.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-965.data"
# $Id: $
