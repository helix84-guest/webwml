msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:52+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "scrisoare de delegare"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "scrisoare de numire în funcție"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegat"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "actual&#259;"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "membru"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "manager"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Administratorul Lansării Distribuției Stabile"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "asistent de configurare"

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "președinte"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "asistent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "secretar"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Ofiteri"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distribuție"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
msgid "Publicity team"
msgstr "Echipa de publicitate"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "Suport și Infrastructură"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr "Blenduri Pure Debian"

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Conducător"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Comitet Tehnic"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Secretar"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Proiecte de dezvoltare"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "Arhive FTP"

#: ../../english/intro/organization.data:96
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "Asistenți FTP"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr "Asistenți de configurare FTP"

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr "Backport-uri"

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr "Echipa pentru backport-uri"

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Pachete individuale"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Administrarea lansărilor"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "Echipa de lansare"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Controlul calit&#259;&#355;ii"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Echipa de Instalare Sisteme"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Note de lansare"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "Imagini CD"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Producere"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Testare"

#: ../../english/intro/organization.data:149
msgid "Autobuilding infrastructure"
msgstr "Infrastructură auto-construibilă"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr "Echipa Wanna-build"

#: ../../english/intro/organization.data:159
msgid "Buildd administration"
msgstr "Administrarea de build-uri"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "documentație"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr " Potențiale pachete noi sau pachete ce necesită lucru"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Porturi"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Configurații speciale"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Laptop-uri"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Firewall-uri"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "Sisteme integrate"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Contact de presă"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "Pagini Web"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr "Mobilizare"

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr "Proiectul Debian pentru Femei"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr "Anti-hărțuire"

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "Evenimente"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Comitet Tehnic"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "Program de parteneriat"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "Coordonarea donațiilor hardware"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "Suport pentru utilizatori"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "Sistem de urmărire pentru bug-uri"

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administrare Liste de discuții și Liste de arhive"

#: ../../english/intro/organization.data:387
msgid "New Members Front Desk"
msgstr "Recepție pentru membri noi"

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "Administratori de conturi Debian"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Menținătorii Keyring-ului (PGP și GPG)"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "Echipa de Securitate"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "Pentru Consultanți"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "Pentru Comercianți de CD-uri"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "Regulament"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Administrarea Sistemului"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Dacă ai probleme în a folosi mașinile puse la dispoziție de Debian "
"(incluzând schimbarea parolei sau dacă ai nevoie de software), aceasta este "
"adresa pe care o poți folosi. "

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Dacă ai probleme de hardware cu o mașină Debian, te rugăm sa consulți pagina "
"<a href=\"https://db.debian.org/machines.cgi\">Debian Machines</a>, ce "
"conține informații despre administrarea fiecărei mașini în parte."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator LDAP"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Oglinzi"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "Administrator DNS"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Sistem de urmărire a pachetelor"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Cereri pentru folosirea <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">Mărcii înregistrate</a> "

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Administratori Alioth"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "Debian pentru copii cu vârste între 1 și 99 de ani"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "Debian pentru practică medicală și cercetare"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "Debian pentru educație"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "Debian pentru birouri de avocatură"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "Debian pentru persoane cu dizabilități"

#: ../../english/intro/organization.data:486
msgid "Debian for science and related research"
msgstr "Debian pentru știință și cercetare"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian pentru educație"

#~ msgid "Live System Team"
#~ msgstr "Echipa pentru sisteme live"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Publicity"
#~ msgstr "Publicitate"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Menținătorii Keyring-ului menținătorilor Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Delegați DebConf"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "echipa de securitate"

#, fuzzy
#~ msgid "Release Assistants"
#~ msgstr "note de lansare"

#, fuzzy
#~ msgid "Release Assistants for ``stable''"
#~ msgstr "administratorul lans&#259;rii pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Release Wizard"
#~ msgstr "administratorul lans&#259;rii"

#~ msgid "APT Team"
#~ msgstr "Echipa APT"

#~ msgid "Internal Projects"
#~ msgstr "proiecte interne"

#~ msgid "Mailing List Archives"
#~ msgstr "arhivele listelor de e-mail"

#~ msgid "Installation System for ``stable''"
#~ msgstr "echipa systemului de instalare pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Marketing Team"
#~ msgstr "echipa de securitate"

#~ msgid "Vendors"
#~ msgstr "v&#226;nz&#259;tori"

#, fuzzy
#~ msgid "Release Team for ``stable''"
#~ msgstr "administratorul lans&#259;rii pentru &#8222;stable&#8221;"

#, fuzzy
#~ msgid "Custom Debian Distributions"
#~ msgstr "distribu&#355;ie"

#~ msgid "Alioth administrators"
#~ msgstr "Administratori Alioth"
