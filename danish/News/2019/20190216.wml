#use wml::debian::translation-check translation="fdc485a3cb59ae0b5ef29c7ff90f411b2444ed09"
<define-tag pagetitle>Opdateret Debian 9: 9.8 udgivet</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den ottende opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction arc 					"Retter mappegennemløbsfejl [CVE-2015-9275], nedbrud i arcdie når der kaldes med mere end et variabelparameter og læsning af version 1 af arc-header">
<correction astroml-addons 				"Retter Python 3-afhængigheder">
<correction base-files 					"Opdaterer til denne punktopdatering">
<correction c3p0 					"Retter XML External Entity-sårbarhed [CVE-2018-20433]">
<correction ca-certificates-java 			"Retter generering af midlertidig jvm-*.cfg på armhf">
<correction chkrootkit 					"Retter regulært udtryk til bortfiltrering af dhcpd og dhclient som falske positive fra pakkesniffertesten">
<correction compactheader 				"Opdaterer til at fungere med nyere versioner af Thunderbird">
<correction courier 					"Retter erstatning af @piddir@">
<correction cups 					"Sikkerhedsrettelser [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config 				"Retter opsætning af personlige websider; genaktiverer offlineinstallering af en combi-server, herunder understøttelse af diskløse arbejdsstationer; aktiverer opsætning af Chromium-hjemmeside på installeringstidspunktet og via LDAP">
<correction debian-installer 				"Genopbygger til denne punktopdatering">
<correction debian-installer-netboot-images 		"Genopbygger mod proposed-updates">
<correction debian-security-support 			"Opdaterer forskellige pakkers understøttelsesstatus">
<correction dnspython 					"Retter fejl ved fortolkning af nsec3-bitmap fra tekst">
<correction egg 					"Springer emacsen-install over ved ikke-understøttet xemacs21">
<correction erlang 					"Installer ikke Erlang-tilstand for XEmacs">
<correction espeakup 					"debian/espeakup.service: Retter kompatibilitet med ældre versioner af systemd">
<correction freerdp 					"Retter sikkerhedsproblemer [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788]; tilføjer understøttelse af CredSSP v3 og RDP proto v6">
<correction ganeti-os-noop 				"Retter størrelsesgenkendelse for ikke-blok-enheder">
<correction glibc 					"Retter flere sikkerhedsproblemer [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237]; undgår segmenteringsfejl på CPU'er med AVX512-F; retter en anvendelse efter frigivelse i pthread_create(); kigger efter postgresql i NSS-tjek; retter pthread_cond_wait() i situationen med pshared på ikke-x86.">
<correction gnulib 					"vasnprintf: Retter fejl i forbindelse med overløb af heaphukommelse [CVE-2018-17942]">
<correction gnupg2 					"Undgår nedbrud når der importeres uden en TTY">
<correction graphite-api 				"Retter stavning af RequiresMountsFor i systemd-service">
<correction grokmirror 					"Tilføjer manglende afhængighed af python-pkg-resources">
<correction gvrng 					"Retter rettighedsproblem som forhindrede start af gvrng; genererer korrekte Python-afhængigheder">
<correction ibus 					"Retter flerarkitekturinstallering ved at fjerne gir-pakkens afhængighed af Python">
<correction icinga2 					"Retter at tidsstemplinger blev gemt som lokal tid i PostgreSQL">
<correction intel-microcode 				"Tilføjer akkumulerede rettelser af Westmere EP (signatur 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort 					"Retter Python-afhængigheder">
<correction jdupes 					"Retter potentielt nedbrud på ARM">
<correction kmodpy 					"Fjerner ukorrekt Multi-Arch: samme som python-kmodpy">
<correction libapache2-mod-perl2 			"Tillader ikke &lt;Perl&gt;-afsnit i brugerkontrolleret opsætning [CVE-2011-2767]">
<correction libb2 					"Finder ud af om systemet kan anvende AVX, før det faktisk tages i brug">
<correction libdatetime-timezone-perl 			"Opdaterer medfølgende data">
<correction libemail-address-list-perl 			"Retter lammelsesangrebssårbarhed [CVE-2018-18898]">
<correction libemail-address-perl 			"Retter lammelsesangrebssårbarheder [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod 					"python-gpod: Tilføjer manglende afhængighed af python-gobject-2">
<correction libssh 					"Retter defekt tastaturinteraktiv autentifikation på serversiden">
<correction linux 					"Ny opstrømsudgave; ny opstrømsversion; retter opbygningsfejl på arm64 og mips*; libceph: retter CEPH_FEATURE_CEPHX_V2-tjek i calc_signature()">
<correction linux-igd 					"Får initscript til at kræve $network">
<correction lttng-modules 				"Retter opbygning på linux-rt 4.9-kerner og kerner &gt;= 4.9.0-3">
<correction mistral 					"Retter std.ssh-handling kan blotlægge tilstedeværelse af vilkårlige filer [CVE-2018-16849]">
<correction monkeysign 					"Retter sikkerhedsproblem [CVE-2018-12020]; sender faktisk adskillige mails i stedet for en enkelt">
<correction mpqc 					"Installerer også sc-libtool">
<correction nvidia-graphics-drivers 			"Ny opstrømsudgave">
<correction nvidia-modprobe 				"Ny opstrømsudgave">
<correction nvidia-persistenced 			"Ny opstrømsudgave">
<correction nvidia-settings 				"Ny opstrømsudgave">
<correction nvidia-xconfig 				"Ny opstrømsudgave">
<correction openni2 					"Retter overtrædelse af armhf-baseline, og armel-FTBFS forårsaget af anvendelse af NEON">
<correction openvpn 					"Retter NCP-virkemåde ved TLS-reconnect, forårsagende <q>AEAD Decrypt error: cipher final failed</q>-fejl">
<correction parsedatetime 				"Tilføjer understøttelse af Python 3">
<correction pdns 					"Retter sikkerhedsproblemer [CVE-2018-1046 CVE-2018-10851]; retter MySQL-forespørgsler med stored procedures; retter at backends til LDAP, Lua og OpenDBX ikke kan finde domæner">
<correction pdns-recursor 				"Retter sikkerhedsproblemer [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage 				"Tilføjer manglende afhængighed af gir1.2-gtk-3.0">
<correction postfix 					"Ny stabil opstrømsudgave; undgår fejl i postconf når postfix-instance-generator kører under boot">
<correction postgresql-9.6 				"Ny opstrømsudgave">
<correction postgrey 					"Genopbygning uden ændringer">
<correction pylint-django 				"Retter Python 3-afhængigheder">
<correction python-acme 				"Tilbagefører nyere version til udfasning af tls-sni-01">
<correction python-arpy 				"Retter Python 3-afhængigheder">
<correction python-certbot 				"Tilbagefører nyere version til udfasning af tls-sni-01">
<correction python-certbot-apache 			"Opdaterer til udfasning af tls-sni-01">
<correction python-certbot-nginx 			"Opdaterer til udfasning af tls-sni-01">
<correction python-hypothesis 				"Retter (omvendte) afhængigheder af python3-hypothesis og python-hypothesis-doc">
<correction python-josepy 				"Ny pakke, krævet af Certbot">
<correction pyzo 					"Tilføjer manglende afhængighed af python3-pkg-resources">
<correction r-cran-readxl 				"Retter nedbrudsfejl [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit 					"Flytter dbus og polkit fra Recommends (anbefalinger) til Depends (afhængigheder)">
<correction ruby-rack 					"Retter mulig sårbarhed i forbindelse med udførelse af skripter på tværs af websteder [CVE-2018-16471]">
<correction samba 					"Ny opstrømsudgave; s3:ntlm_auth: retter hukommelseslækage i manage_gensec_request(); ignorer startfejl fra nmbd når der ikke er en ikke-loopback-grænseflade eller ingen lokal IPv4 ikke-loopback-grænseflade; retter CVE-2018-14629-regression ved en ikke-CNAME-post">
<correction sl-modem 					"Understøtter Linux-versioner &gt; 3">
<correction sogo-connector 				"Opdaterer til at fungere med nyere versioner af Thunderbird">
<correction sox 					"Anvender faktisk rettelser af CVE-2014-8145">
<correction ssh-agent-filter 				"Retter to byte-stakskrivning udenfor grænserne">
<correction supercollider 				"Deaktiverer understøttelse af XEmacs og Emacs &lt;=23">
<correction sympa 					"Fjerner /etc/sympa/sympa.conf-smime.in fra conffiles; anvender komplet sti til headkommando i Sympas opsætningsfil">
<correction twitter-bootstrap3 				"Retter adskillige sikkerhedssårbarheder [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata 					"Ny opstrømsudgave">
<correction uglifyjs 					"Retter indhold af manpage">
<correction uriparser 					"Retter adskillige sikkerhedssårbarheder [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm 						"Dropper understøttelse af xemacs21">
<correction vulture 					"Tilføjer manglende afhængighed af python3-pkg-resources">
<correction wayland 					"Retter muligt heltalsoverløb [CVE-2017-16612]">
<correction wicd 					"Vær altid afhængig af net-tools, frem for alternativer">
<correction wvstreams 					"Omgår stakkorruption">
<correction xapian-core 				"Retter lækager af freelist-blokke under særlige omstændigheder, som dernæst rapporteredes som <q>DatabaseCorruptError</q> af Database::check()">
<correction xkeycaps 					"Forhindrer segmenteringsfejl i commands.c når der er flere end otte keysyms pr. nøgle til stede">
<correction yosys 					"Retter <q>ModuleNotFoundError: Intet modul kaldet <q>smtio</q></q>">
<correction z3 						"Fjerner ukorrekt Multi-Arch: samme som python-z3">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction adblock-plus 			"Inkompatibel med nyere firefox-esr versions">
<correction calendar-exchange-provider 		"Inkompatibel med nyere firefox-esr versions">
<correction cookie-monster 			"Inkompatibel med nyere firefox-esr versions">
<correction corebird 				"Defekt efter ændringer af Twitter-API">
<correction debian-buttons 			"Inkompatibel med nyere firefox-esr versions">
<correction debian-parl 			"Afhængig af defekte/fjernede Firefox-plugins">
<correction firefox-branding-iceweasel 		"Inkompatibel med nyere firefox-esr versions">
<correction firefox-kwallet5 			"Inkompatibel med nyere firefox-esr versions">
<correction flashblock 				"Inkompatibel med nyere firefox-esr versions">
<correction flickrbackup 			"Inkompatibel med aktuelt Flickr-API">
<correction imap-acl-extension 			"Inkompatibel med nyere firefox-esr versions">
<correction libwww-topica-perl 			"Nytteløs efter lukningen af webstedet Topica">
<correction mozilla-dom-inspector 		"Inkompatibel med nyere firefox-esr versions">
<correction mozilla-noscript 			"Inkompatibel med nyere firefox-esr versions">
<correction mozilla-password-editor 		"Inkompatibel med nyere firefox-esr versions">
<correction mozvoikko 				"Inkompatibel med nyere firefox-esr versions">
<correction personaplus 			"Inkompatibel med nyere firefox-esr versions">
<correction python-formalchemy 			"Ubrugelig, kan ikke importeres i Python">
<correction refcontrol 				"Inkompatibel med nyere firefox-esr versions">
<correction requestpolicy 			"Inkompatibel med nyere firefox-esr versions">
<correction spice-xpi 				"Inkompatibel med nyere firefox-esr versions">
<correction toggle-proxy 			"Inkompatibel med nyere firefox-esr versions">
<correction y-u-no-validate 			"Inkompatibel med nyere firefox-esr versions">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://security.debian.org/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
