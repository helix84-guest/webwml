#use wml::debian::translation-check translation="399626927b999b3938582bfb0243645dfed48f14"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron múltiples vulnerabilidades en cURL, una biblioteca para transferencia de URL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16890">CVE-2018-16890</a>

    <p>Wenxiang Qian, del Tencent Blade Team, descubrió que la función
    que gestiona los mensajes NTLM tipo 2 entrantes no valida correctamente
    los datos entrantes y está expuesta a una vulnerabilidad de desbordamiento de entero
    que podría dar lugar a lectura de memoria fuera de límites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3822">CVE-2019-3822</a>

    <p>Wenxiang Qian, del Tencent Blade Team, descubrió que la función
    que crea las cabeceras NTLM tipo 3 salientes está expuesta a una vulnerabilidad
    de desbordamiento de entero que podría dar lugar a escritura fuera de límites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3823">CVE-2019-3823</a>

    <p>Brian Carpenter, de Geeknik Labs, descubrió que el código que gestiona
    los end-of-response SMTP está expuesto a una lectura de memoria dinámica («heap») fuera de
    límites.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 7.52.1-5+deb9u9.</p>

<p>Le recomendamos que actualice los paquetes de curl.</p>

<p>Para información detallada sobre el estado de seguridad de curl, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4386.data"
