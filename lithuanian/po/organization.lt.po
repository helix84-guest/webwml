msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-22 15:31+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "dabartinis"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "narys"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "vedėjas"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
#| msgid "Release Managers"
msgid "Stable Release Manager"
msgstr "Išleidimo Valdytojai"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "vedlys"

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "pirmininkas"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "asistentas"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "sekretorius"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Pareigūnai"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Viešieji_ryšiai"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "Palaikymas ir Infrastruktūra"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Lyderis"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Techninis Komitetas"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Sekretorius"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Plėtojimo Projektai"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "FTP Archyvai"

#: ../../english/intro/organization.data:96
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "FTP Meistras"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "FTP Asistentai"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Individualūs paketai"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Vadovavimas Išleidimui"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "Išleidimo komanda"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Kokybės Užtikrinimas"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Įdiegimo sistemos komanda"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Išleidimo pastabos"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD atvaizdai"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Produkcija"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Testuojama"

#: ../../english/intro/organization.data:149
#, fuzzy
#| msgid "Support and Infrastructure"
msgid "Autobuilding infrastructure"
msgstr "Palaikymas ir Infrastruktūra"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
#, fuzzy
#| msgid "Buildd Administration"
msgid "Buildd administration"
msgstr "Buildd Administracija"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "Reikalaujantis darbo ir Laukiamas paketų sąrašas"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Pernešimai"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Specialios konfigūracijos"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Nešiojami kompiuteriai"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Saugos siena"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "Įdiegtos sistemos"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Ryšiai su žiniasklaida"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "Tinkalalapiai "

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "Debian Planeta"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr "Moterys Debian projekte"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "Įvykiai"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Techninis Komitetas"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "Partnerio Programa"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "Kompiuterinės įrangos aukojimo koordinatorius"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "Vartotojų palaikymas"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "Klaidų stebėjimo sistema"

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Pašto konferencijų administracija ir Pašto konferencijų archyvai"

#: ../../english/intro/organization.data:387
#, fuzzy
#| msgid "New Maintainers Front Desk"
msgid "New Members Front Desk"
msgstr "Naujo plėtojo registracinis stalas"

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "Debian sąskaitos valdytojai"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Raktinės valdytojai (PGP ir GPG)"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "Saugumo komanda"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "Konsultantų puslapis"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "CD Pardavėjų puslapis"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "Politika"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Sistemos administracija"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Tai yra adresas, kurį naudoti jei iškilus problemoms su Debian mašinoms, "
"įskaitant slaptažodžio problemas arba jie jums reikia įdiegto paketo."

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Jeigu jūs turit problemų su kompiuterine įranga naudojant Debian, prašom "
"žiūrėti <a href=\"https://db.debian.org/machines.cgi\"> Debian Mašinos</a> "
"puslapį, jame turi būti mašinos administratoriaus informacija."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP plėtotojų direktorijos administratorius"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Veidrodžiai"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "DNS prižiūrėtojas"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Paketų stebėjimo sistema"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Alioth administratoriai"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "Debian vaikams nuo 1 iki 99"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "Debian medicinos praktikai ir jų tyrimams"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "Debian švietimui"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "Debian teisėje"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "Debian neįgaliems žmonėms"

#: ../../english/intro/organization.data:486
#, fuzzy
#| msgid "Debian for medical practice and research"
msgid "Debian for science and related research"
msgstr "Debian medicinos praktikai ir jų tyrimams"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian švietimui"

#~ msgid "Live System Team"
#~ msgstr "Live sistemos komanda"

#~ msgid "Auditor"
#~ msgstr "Revizorius"

#~ msgid "Publicity"
#~ msgstr "Viešieji_ryšiai"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian plėtotųjų raktinės komanda"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Papildomos Debian distribucijos"

#~| msgid "Release Manager for ``stable''"
#~ msgid "Release Team for ``stable''"
#~ msgstr "``stable'' Išleidimo komanda"

#~ msgid "Vendors"
#~ msgstr "Pardavėjai"

#~ msgid "Handhelds"
#~ msgstr "Kišeniniai kompiuteriai"

#~| msgid "Security Testing Team"
#~ msgid "Marketing Team"
#~ msgstr "Prekybos Komanda"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratoriai, atsakingi už tam tikra architektūrą gali būti "
#~ "pasiekiami <genericemail arch@buildd.debian.org>, pavyzdžiui "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Individualių buildd administratorių vardus galite rasti taip pat čia href="
#~ "\"http://www.buildd.net\">http://www.buildd.net</a>. Pasirinkite "
#~ "architektūrą ir distributyvą kad pažiūrėtumėte galimus buildd ir jų "
#~ "administratorius."

#~ msgid "Key Signing Coordination"
#~ msgstr "Rakto pasirašymo koordinatorius"

#~ msgid "Accountant"
#~ msgstr "Sąskaitininkas"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Universali operacinė sistema jūsų darbastalyje"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian pelno nesiekiančioms organizacijoms"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Tai dar ne oficialus vidinis Debian projektas, bet jis paskelbė siekti "
#~ "integraciją."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian Multimedijos Distributyvas"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux Verslui"

#~ msgid "Mailing List Archives"
#~ msgstr "Pašto konferencijos archyvai"

#~ msgid "APT Team"
#~ msgstr "APT komanda"

#~ msgid "Release Assistants"
#~ msgstr "Išleidimo Asistentai"

#~ msgid "Security Audit Project"
#~ msgstr "Saugumo revizijos projektas"

#~ msgid "Testing Security Team"
#~ msgstr "Testing Saugumo komanda"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth administratoriai"
